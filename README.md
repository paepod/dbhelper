##Introduction

This helper is extenion method for `SqlConnection` 

You may execute normal sql statments. DBHelper is allowed you to use `Execute*` methods to execute the statments. The suffix method name is the return type of the its methods.

	ExecuteDataTable(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataTable
	ExecuteListDataTable(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As List(Of DataTable)
	ExecuteDataset(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataSet
   
You may call stored procedure. DBHelper provides you `ExecuteSp*` method to call stored procedure:

	ExecuteSpDataTable(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataTable
	ExecuteSpListDataTable(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As List(Of DataTable)
	ExecuteSpDataset(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataSet
	
Perhaps you need transaction. DBHelper provides method for you to do database transaction:
 	**Note You must use StartTransaction() instead of BeginTransaction()**
	 
	StartTransaction() As SqlTransaction  
	EndTransaction()
	Commit()
	Rollback()

Sometime you may execute sql operation that returns row effected by using `ExecuteNonQuery` and `ExecuteSpNonQuery`

	ExecuteNonQuery(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As Integer
	ExecuteSpNonQuery(ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As Integer

`ToSqlParameters` method is allowed you to convert `Anonymous Object` into `SqlParameter`

	ToSqlParameters(values As Object) As SqlParameter()

##Examples
Here is the examples:

	Sub Main()
	
        ' Initialize
        Dim result = 0
        Dim createTable = "CREATE TABLE [dbo].[MyTable] ( [StrangerID] Int NULL, [Color] NVARCHAR(MAX) NULL, [Wavelength] Int NULL)"
        Dim dropTable = "IF OBJECT_ID('MyTable', 'U') IS NOT NULL DROP TABLE MyTable;"
        Dim dropProc = "IF OBJECT_ID('MyProc', 'P') IS NOT NULL DROP PROC MyProc;"
        Dim createProc = "CREATE PROCEDURE [dbo].[MyProc] @param1 int = 0 OUTPUT As Set @param1 = @param1 + 1 Return 9999"

        ' You need to identify the connection string 
        Dim connStr = "Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=datatest;Integrated Security=True;"

        ' Then pass it into SqlConnection instance.
        Dim conn As New SqlConnection(connStr)

        ' Execute sql statement
        result = conn.ExecuteNonQuery(dropTable)
        result = conn.ExecuteNonQuery(createTable)
        result = conn.ExecuteNonQuery(dropProc)
        result = conn.ExecuteNonQuery(createProc)
		
		' You might execute non-query statments such as INSERT or UPDATE as follows,
		conn.ExecuteNonQuery("INSERT INTO dbo.MyTable (StrangerID, Color, Wavelength) VALUES(@p3,@p1,@p2)", conn.ToSqlParameters(New With {.p1 = "PINK", .p2 = 1, .p3 = 2}))	
		conn.ExecuteNonQuery("UPDATE dbo.MyTable Set Color = @p1, Wavelength = @p2 WHERE StrangerID = @p3", conn.ToSqlParameters(New With {.p1 = "SKY", .p2 = 1, .p3 = 1}))
		
		' We are Going to call `ExecuteDataset` that return Dataset instance
		Dim ds = conn.ExecuteDataset("SELECT * FROM dbo.MyTable")

		' If you want to pass SqlParameter you can do like below:
		Dim Table = conn.ExecuteDataTable("SELECT * FROM dbo.MyTable WHERE StrangerID = @id", conn.ToSqlParameters(New With {.id = 1}))
		
		' or passing via SqlParameter instance , below example also shows how to call 2 sqls whiic it will return List(Of Table)
		Dim Tables2 = conn.ExecuteListDataTable("SELECT * FROM dbo.MyTable WHERE StrangerID = @id;SELECT * FROM dbo.MyTable WHERE StrangerID = @id2",
                                                   New SqlParameter("@id", 1), New SqlParameter("@id2", 2))
		
		' Perhap you call stored procedure with OUT paratmer and RETURN_VALUE parameter, you can do it as follows,

		' Create a InputOutput parameter
        Dim param1 = New SqlParameter()
        param1.ParameterName = "@param1"
        param1.Value = 10
		param1.Direction = ParameterDirection.InputOutput

		' Create a RETURN_VALUE parameter
        Dim paramReturnValue = New SqlParameter()
		'SQL SERVER use @RETURN_VALUE as name of ReturnValue. You must do  the same  as this example if you need ReturnValue
        paramReturnValue.ParameterName = "@RETURN_VALUE"
        paramReturnValue.Direction = ParameterDirection.ReturnValue

		' Display both parameters
        Console.WriteLine(param1.Value) '10
        Console.WriteLine(paramReturnValue.Value) 'empty string

		' Execute stored procedure with both parameters
        conn.ExecuteSpNonQuery("[dbo].[MyProc]", param1, paramReturnValue)
		
        Console.WriteLine(param1.Value) '11
        Console.WriteLine(paramReturnValue.Value) '9999
		
	End Sub
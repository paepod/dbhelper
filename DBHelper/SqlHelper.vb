﻿Imports System.Runtime.CompilerServices
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Reflection


Module SqlHelper

    Private _IsTransaction As Boolean = False
    Private _transaction As SqlTransaction

    <Extension()>
    Public Function ToSqlParameters(ByVal Conn As SqlConnection, values As Object) As SqlParameter()
        If values IsNot Nothing Then
            Dim Properties As PropertyInfo() = values.GetType().GetProperties()
            Dim CommandParameters(Properties.Length - 1) As SqlParameter
            For i = LBound(Properties) To UBound(Properties)

                Dim paramValue As Object = Properties(i).GetValue(values, Nothing)
                Dim paramName As String = If(Properties(i).Name.StartsWith("@", StringComparison.Ordinal), Properties(i).Name, "@" + Properties(i).Name)

                Dim param As New SqlParameter(paramName, paramValue)

                CommandParameters(i) = param
            Next

            Return CommandParameters
        End If

        Return Nothing
    End Function

    <Extension()>
    Public Function ExecuteSpNonQuery(ByVal conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As Integer
        Return ExecuteNonQuery(conn, CommandText, CommandType.StoredProcedure, CommandParameters)
    End Function

    <Extension()>
    Public Function ExecuteSpDataTable(ByVal Conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataTable
        Dim ds = ExecuteDataset(Conn, CommandText, CommandType.StoredProcedure, CommandParameters)
        Return ds.Tables(0)
    End Function

    <Extension()>
    Public Function ExecuteSpListDataTable(ByVal Conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As List(Of DataTable)
        Dim dataset = ExecuteSpDataset(Conn, CommandText, CommandParameters)

        Dim list As New List(Of DataTable)
        For Each data As DataTable In dataset.Tables
            list.Add(data)
        Next

        Return list
    End Function

    <Extension()>
    Public Function ExecuteSpDataset(ByVal Conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataSet
        Return ExecuteDataset(Conn, CommandText, CommandType.StoredProcedure, CommandParameters)
    End Function

    <Extension()>
    Public Function ExecuteDataTable(ByVal Conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataTable
        Dim ds = ExecuteDataset(Conn, CommandText, CommandParameters)

        Return ds.Tables(0)
    End Function

    <Extension()>
    Public Function ExecuteListDataTable(ByVal Conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As List(Of DataTable)
        Dim dataset = ExecuteDataset(Conn, CommandText, CommandParameters)

        Dim list As New List(Of DataTable)
        For Each data As DataTable In dataset.Tables
            list.Add(data)
        Next

        Return list
    End Function

    <Extension()>
    Public Function ExecuteDataset(ByVal Conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As DataSet
        Return ExecuteDataset(Conn, CommandText, CommandType.Text, CommandParameters)
    End Function

    <Extension()>
    Public Function ExecuteNonQuery(ByVal conn As SqlConnection, ByVal CommandText As String, ParamArray CommandParameters As SqlParameter()) As Integer
        Return ExecuteNonQuery(conn, CommandText, CommandType.Text, CommandParameters)
    End Function

    Private Function ExecuteNonQuery(ByVal conn As SqlConnection, ByVal CommandText As String,
                                 ByVal CommandType As CommandType, ParamArray CommandParameters As SqlParameter()) As Integer
        OpenConnection(conn)

        Dim result As Integer = 0

        Using cmd As New SqlCommand()

            SetCommand(conn, cmd, CommandText, CommandType)
            AddParameter(CommandParameters, cmd)

            result = cmd.ExecuteNonQuery()

            cmd.Parameters.Clear()

            If (_IsTransaction = False) Then
                conn.Close()
            End If

        End Using

        Return result
    End Function

    <Extension()>
    Public Function StartTransaction(ByVal conn As SqlConnection) As SqlTransaction
        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        conn.Open()
        _IsTransaction = True
        _transaction = conn.BeginTransaction()
        Return _transaction
    End Function

    <Extension()>
    Public Sub EndTransaction(ByVal conn As SqlConnection)
        Commit(conn)
    End Sub

    <Extension()>
    Public Sub Commit(ByVal conn As SqlConnection)
        _transaction.Commit()
        _IsTransaction = False
        conn.Close()
    End Sub

    <Extension()>
    Public Sub Rollback(ByVal conn As SqlConnection)
        _transaction.Rollback()
        _IsTransaction = False
        conn.Close()
    End Sub

    Private Function ExecuteDataset(ByVal conn As SqlConnection, ByVal CommandText As String,
                                ByVal CommandType As CommandType, ParamArray CommandParameters As SqlParameter()) As DataSet
        OpenConnection(conn)
        'create the Dataset
        Dim ds As New DataSet()

        Using cmd As New SqlCommand()
            SetCommand(conn, cmd, CommandText, CommandType)
            AddParameter(CommandParameters, cmd)

            'create the DataAdapter
            Dim da As New SqlDataAdapter(cmd)

            'fill the DataSet using default values for DataTable names, etc.
            da.Fill(ds)

            ' detach the SqlParameters from the command object, so they can be used again.			
            cmd.Parameters.Clear()
        End Using

        'return the dataset
        Return ds
    End Function

    Private Sub SetCommand(conn As SqlConnection, cmd As SqlCommand, commandText As String, commandType As CommandType)
        With cmd
            .Connection = conn
            .CommandText = commandText
            .CommandType = commandType
        End With

        If (_IsTransaction) Then
            cmd.Transaction = _transaction
        End If
    End Sub

    Private Sub OpenConnection(conn As SqlConnection)
        If (_IsTransaction = False) Then
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            conn.Open()
        End If
    End Sub

    Private Sub AddParameter(CommandParameters() As SqlParameter, cmd As SqlCommand)
        If CommandParameters IsNot Nothing Then
            For Each p As DbParameter In CommandParameters
                cmd.Parameters.Add(p)
            Next
        End If
    End Sub

End Module
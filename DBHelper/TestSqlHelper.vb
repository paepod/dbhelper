﻿Imports System.Data.SqlClient
Imports DBHelper
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace Test

    <TestClass()>
    Public Class TestSqlHelper
        Private connStr As String

        <TestInitialize()>
        Public Sub Initialize()
            connStr = "Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=datatest;Integrated Security=True;"
        End Sub



        <TestMethod()>
        Public Sub DMLAndDDLTest()
            Dim conn = New SqlConnection(connStr)
            Dim tableName = "MyTable"
            Dim result As Integer = 0
            Dim createTable = GetCreateTable(tableName)
            Dim dropTable = GetDropTable(tableName)

            result = conn.ExecuteNonQuery(dropTable)
            Assert.AreEqual(0, 0)
            result = conn.ExecuteNonQuery(createTable)
            Assert.AreEqual(0, 0)


            result = conn.ExecuteNonQuery(String.Format("INSERT INTO dbo.{0} (StrangerID, Color, Wavelength) VALUES(@p3,@p1,@p2)", tableName),
                     conn.ToSqlParameters(New With {.p1 = "BLUE", .p2 = 1, .p3 = 1}))

            Assert.AreEqual(1, result)

            result = conn.ExecuteNonQuery(String.Format("INSERT INTO dbo.{0} (StrangerID, Color, Wavelength) VALUES(@p3,@p1,@p2)", tableName),
                     conn.ToSqlParameters(New With {.p1 = "PINK", .p2 = 1, .p3 = 2}))

            Assert.AreEqual(1, result)

            result = conn.ExecuteNonQuery(String.Format("UPDATE dbo.{0} Set Color = @p1, Wavelength = @p2 WHERE StrangerID = @p3", tableName),
                     conn.ToSqlParameters(New With {.p1 = "SKY", .p2 = 1, .p3 = 1}))

            Assert.AreEqual(1, result)

            Dim Table = conn.ExecuteDataTable(String.Format("Select * FROM dbo.{0} WHERE StrangerID = @id", tableName),
                                                   conn.ToSqlParameters(New With {.id = 1, .id2 = 2}))

            Assert.AreEqual(1, Table.Rows.Count)

            Dim ds = conn.ExecuteDataset(String.Format("Select * FROM dbo.{0} WHERE StrangerID = @id;Select * FROM dbo.{0} WHERE StrangerID = @id2", tableName),
                                                   conn.ToSqlParameters(New With {.id = 1, .id2 = 2}))
            Assert.AreEqual(2, ds.Tables.Count)

            Dim Tables = conn.ExecuteListDataTable(String.Format("Select * FROM dbo.{0} WHERE StrangerID = @id;Select * FROM dbo.{0} WHERE StrangerID = @id2", tableName),
                                                   conn.ToSqlParameters(New With {.id = 1, .id2 = 2}))

            Assert.AreEqual(2, Tables.Count)

            Dim Tables2 = conn.ExecuteListDataTable(String.Format("Select * FROM dbo.{0} WHERE StrangerID = @id;Select * FROM dbo.{0} WHERE StrangerID = @id2", tableName),
                                                   New SqlParameter("@id", 1), New SqlParameter("@id2", 2))

            Assert.AreEqual(2, Tables2.Count)

            conn.ExecuteNonQuery(dropTable)
        End Sub

        <TestMethod()>
        Public Sub SpTest()
            Dim conn = New SqlConnection(connStr)
            Dim spName = "MyProc"
            Dim createSp = GetCreateSp(spName)
            Dim dropSp = GetDropSp(spName)


            Dim result = conn.ExecuteNonQuery(dropSp)
            Assert.AreEqual(0, 0)

            result = conn.ExecuteNonQuery(createSp)
            Assert.AreEqual(0, 0)

            Dim param1 = New SqlParameter()
            param1.ParameterName = "@param1"
            param1.Value = 10
            param1.Direction = ParameterDirection.InputOutput


            Dim paramReturnValue = New SqlParameter()
            paramReturnValue.ParameterName = "@RETURN_VALUE"
            paramReturnValue.Direction = ParameterDirection.ReturnValue



            conn.ExecuteSpNonQuery("[dbo].[MyProc]", param1, paramReturnValue)

            Assert.AreEqual(11, param1.Value)
            Assert.AreEqual(9999, paramReturnValue.Value)

            conn.ExecuteNonQuery(dropSp)
        End Sub

        <TestMethod()>
        Public Sub TransactionRollBackTest()
            Dim conn = New SqlConnection(connStr)
            Dim tableName = "RollbackTransaction"
            Dim result As Integer = 0
            Dim createTable = GetCreateTable(tableName)
            Dim dropTable = GetDropTable(tableName)

            result = conn.ExecuteNonQuery(dropTable)
            Assert.AreEqual(0, 0)
            result = conn.ExecuteNonQuery(createTable)
            Assert.AreEqual(0, 0)

            conn.StartTransaction()
            result = conn.ExecuteNonQuery(String.Format("INSERT INTO dbo.{0} (StrangerID, Color, Wavelength) VALUES(@p3,@p1,@p2)", tableName),
                     conn.ToSqlParameters(New With {.p1 = "BLUE", .p2 = 1, .p3 = 1}))
            Assert.AreEqual(1, result)

            result = conn.ExecuteNonQuery(String.Format("INSERT INTO dbo.{0} (StrangerID, Color, Wavelength) VALUES(@p3,@p1,@p2)", tableName),
                     conn.ToSqlParameters(New With {.p1 = "PINK", .p2 = 1, .p3 = 2}))
            Assert.AreEqual(1, result)

            result = conn.ExecuteNonQuery(String.Format("UPDATE dbo.{0} Set Color = @p1, Wavelength = @p2 WHERE StrangerID = @p3", tableName),
                     conn.ToSqlParameters(New With {.p1 = "SKY", .p2 = 1, .p3 = 1}))
            Assert.AreEqual(1, result)

            Assert.AreEqual(ConnectionState.Open, conn.State)
            conn.Rollback()
            Assert.AreEqual(ConnectionState.Closed, conn.State)

            Dim Table = conn.ExecuteDataTable(String.Format("Select * FROM dbo.{0} WHERE StrangerID = @id", tableName), New SqlParameter("@id", 1))
            Assert.AreEqual(0, Table.Rows.Count)
            conn.ExecuteNonQuery(dropTable)

        End Sub

        <TestMethod()>
        Public Sub TransactionCommitTest()
            Dim dt As DataTable
            Dim conn = New SqlConnection(connStr)
            Dim tableName = "CommitTransaction"
            Dim result As Integer = 0
            Dim createTable = GetCreateTable(tableName)
            Dim dropTable = GetDropTable(tableName)

            result = conn.ExecuteNonQuery(dropTable)
            Assert.AreEqual(0, 0)
            result = conn.ExecuteNonQuery(createTable)
            Assert.AreEqual(0, 0)


            conn.StartTransaction()
            conn.ExecuteNonQuery(String.Format("INSERT INTO dbo.{0} (StrangerID, Color, Wavelength) VALUES(@p3,@p1,@p2)", tableName),
                     conn.ToSqlParameters(New With {.p1 = "BLUE", .p2 = 1, .p3 = 1}))


            dt = conn.ExecuteDataTable(String.Format("Select * FROM dbo.{0} WHERE StrangerID = @id", tableName), New SqlParameter("@id", 1))
            Assert.AreEqual(1, dt.Rows.Count)

            conn.Commit()
            dt = conn.ExecuteDataTable(String.Format("Select * FROM dbo.{0} WHERE StrangerID = @id", tableName), New SqlParameter("@id", 1))
            Assert.AreEqual(1, dt.Rows.Count)

            conn.StartTransaction()
            conn.ExecuteNonQuery(String.Format("INSERT INTO dbo.{0} (StrangerID, Color, Wavelength) VALUES(@p3,@p1,@p2)", tableName),
                     conn.ToSqlParameters(New With {.p1 = "GREEN", .p2 = 1, .p3 = 2}))


            dt = conn.ExecuteDataTable(String.Format("Select * FROM dbo.{0}", tableName))
            Assert.AreEqual(2, dt.Rows.Count)

            Assert.AreEqual(ConnectionState.Open, conn.State)

            conn.EndTransaction()

            Assert.AreEqual(ConnectionState.Closed, conn.State)

            dt = conn.ExecuteDataTable(String.Format("Select * FROM dbo.{0}", tableName))
            Assert.AreEqual(2, dt.Rows.Count)

            conn.ExecuteNonQuery(dropTable)
        End Sub

        Private Function GetCreateTable(ByVal tableName As String) As String
            Return String.Format("CREATE TABLE [dbo].[{0}] ( [StrangerID] Int NULL, [Color] NVARCHAR(MAX) NULL, [Wavelength] Int NULL)", tableName)
        End Function

        Private Function GetCreateSp(ByVal spName As String) As String
            Return String.Format("CREATE PROCEDURE [dbo].[{0}] @param1 int = 0 OUTPUT As Set @param1 = @param1 + 1 Return 9999", spName)
        End Function

        Private Function GetDropSp(ByVal spName As String) As String
            Return String.Format("IF OBJECT_ID('{0}', 'P') IS NOT NULL DROP PROC {0};", spName)
        End Function

        Private Function GetDropTable(ByVal tableName As String) As String
            Return String.Format("IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = '{0}' AND TABLE_SCHEMA = 'dbo')" &
                 Environment.NewLine & "DROP TABLE dbo.{0};", tableName)
        End Function
    End Class
End Namespace
